Just a simple link shortener


# Start
```
npm i
npm run webpack
npm run dev
```

# Watch
```
npm run webpack -- --watch
```

# TODO:
- add index on links

# Security / Scaling
- input link is being validated from UI and from Backend Service
- preferred short reference has been sanitized from special characters
- service easily can fit into micro services architecture to handle the scalability 
- In case of micro services, Backend endpoint for generating short link can easily integrate into graphQL as a Mutation

# Local Postgres
```
docker-compose up
```

# Fandogh (PAAS) Setup
```
# Managed DB (Postgres)
fandogh managed-service deploy postgresql 10.4 -c postgres_password=linkshortener

# Preparing Image
fandogh image init --name link-shortener
fandogh image publish -v v0.1

# Deploy the Service
fandogh service deploy \
  --image link-shortener \
  --version v0.4 \
  --name link-shortener \
  --port 4000 \
  --env NODE_ENV=development \
  --env DB_HOST=postgresql \
  --env DB_DATABASE=postgres \
  --env DB_USERNAME=postgres \
  --env DB_PASSWORD=linkshortener

# Destroy the Services
fandogh service destroy --name postgresql
fandogh service destroy --name link-shortener

```
