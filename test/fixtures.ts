export const insertLinkQuery = `
  INSERT INTO links(id, url, short_reference, registered_at)
    VALUES (1, 'https://facebook.com', 'fblink', '2018-07-09 18:12:34.944 +00:00');
`

export const truncate = `TRUNCATE TABLE "links" CASCADE;`
