import * as request from 'supertest'
import * as ShortHash from 'shorthash'
import * as fixtures from './fixtures'

import { Config, Express } from '../src/types/link-shortener'
import App from '../src/app'

const config = require('../src/configs/test') as Config

let app, shortHashSpy
describe('App', () => {
  async function truncate () {
    await app.sequelize.query(fixtures.truncate)
  }

  beforeAll(async () => {
    app = await App(config) as Express
  })

  beforeEach(async () => {
    await truncate()
  })

  afterAll(async () => {
    await app.close()
  })

  describe('[GET] /health-check', () => {
    it('should do health check', async () => {
      const response: any = await request(app)
        .get('/health-check')

      expect(response.statusCode).toBe(200)
      expect(response.body).toEqual({
        success: true
      })
    })
  })

  describe('[GET] /', () => {
    it('should show the landing', async () => {
      const response: any = await request(app)
        .get('/')

      expect(response.statusCode).toBe(200)
    })
  })

  describe('[GET] /:shortReference', () => {
    it('should return 404 if shortReference does not exists', async () => {
      const response: any = await request(app)
        .get('/test')

      expect(response.statusCode).toBe(404)
    })

    it('should redirect to google', async () => {
      await app.sequelize.query(fixtures.insertLinkQuery)

      const response: any = await request(app)
        .get('/fblink')

      expect(response.statusCode).toBe(302)
      expect(response.headers.location).toBe('https://facebook.com')
    })
  })

  describe('[POST] /generate-new-link', () => {
    beforeAll(async () => {
      shortHashSpy = jest.spyOn(ShortHash, 'unique').mockImplementation(() => 'ZWWaNw')

      app = await App(config) as Express
    })

    beforeEach(async () => {
      await truncate()
    })

    afterAll(async () => {
      await app.close()
      await shortHashSpy.mockReset()
      await shortHashSpy.mockRestore()
    })

    it('should return 400 if url is not valid', async () => {
      const response: any = await request(app)
        .post('/generate-new-link')
        .set('Content-Type', 'application/json')
        .send({
          url: 'wrongUrlFormat',
          preferredShortReference: false
        })

      expect(response.statusCode).toBe(400)
      expect(response.body).toEqual({
        error: 'url<wrongUrlFormat> is not valid'
      })
    })

    it('should return link object', async () => {
      const response: any = await request(app)
        .post('/generate-new-link')
        .set('Content-Type', 'application/json')
        .send({
          url: 'https://facebook.com',
          preferredShortReference: false
        })

      expect(response.statusCode).toBe(200)
      expect(response.body).toEqual(expect.objectContaining({
        shortReference: 'ZWWaNw',
        url: 'https://facebook.com',
      }))
    })

    it('should return link object with prefered short reference', async () => {
      const response: any = await request(app)
        .post('/generate-new-link')
        .set('Content-Type', 'application/json')
        .send({
          url: 'https://facebook.com',
          preferredShortReference: 'fblink'
        })

      expect(response.statusCode).toBe(200)
      expect(response.body).toEqual(expect.objectContaining({
        shortReference: 'fblink',
        url: 'https://facebook.com',
      }))
    })

    it('should return link object with random short reference if preferredShortReference is already exists', async () => {
      await app.sequelize.query(fixtures.insertLinkQuery)

      const response: any = await request(app)
        .post('/generate-new-link')
        .set('Content-Type', 'application/json')
        .send({
          url: 'https://facebook.com',
          preferredShortReference: 'fblink'
        })

      expect(response.statusCode).toBe(200)
      expect(response.body).toEqual(expect.objectContaining({
        shortReference: 'ZWWaNw',
        url: 'https://facebook.com',
      }))
    })
  })
})
