import * as React from 'react'
import LandingPageController from '../controllers/landing-page'

export interface LandingPageState {
  isPreferredShortReferenceAppears: Boolean
  shortedLinks: Array<ShortedLinkEntity>
}

export interface ShortedLinkEntity {
  url: string
  shortedLink: string
}

export default class LandingPage extends LandingPageController {
  constructor (props: any) {
    super(props)

    this.state = {
      isPreferredShortReferenceAppears: false,
      shortedLinks: []
    } as LandingPageState
  }

  renderPreferredShortReference () {
    if (!this.state.isPreferredShortReferenceAppears) {
      return <label className='form-check-label pt-1 pb-1' htmlFor='prefer-checkbox'>I'd like to customize my link</label>
    }

    return <input className='form-control form-control-sm' placeholder='/custom-link' ref='preferredShortReference' />
  }

  renderShortedLinks () {
    return this.state.shortedLinks.map((link, key) => <div key={key} className='alert alert-success'>
      <strong><a target='blank' href={link.shortedLink}>{link.shortedLink}</a></strong> <span className='text-muted'>{link.url}</span>
    </div>)
  }

  render() {
    return (
      <div className='container'>
        <div className='row pt-5'>
          <div className='offset-md-3 col-md-6'>
            <form onSubmit={this.onShortify.bind(this)}>
              <h1>Link Shortener</h1>
              <div className='align-middle'>
                <input className='form-control' placeholder='Place your Link here' ref='url' />
                <button type='submit' className='btn btn-success mb-2 float-right mt-1'>Shortify</button>
              </div>
              <div className='form-inline mt-2'>
                <input type='checkbox' className='form-check-input' id='prefer-checkbox' onChange={this.togglePreferredShortReference.bind(this)} />
                {this.renderPreferredShortReference()}
              </div>
            </form>
            <div className='clearfix' />
            {this.renderShortedLinks()}
          </div>
        </div>
      </div>
    )
  }
}
