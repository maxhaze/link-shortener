import * as React from 'react'
import * as ReactDOM from 'react-dom'
import LandingPage from './pages/landing-page'

ReactDOM.render(
  <LandingPage />,
  document.getElementById('app')
)
