import * as React from 'react'
import * as Url from 'url'
import Fetch from 'node-fetch'
import { LandingPageState } from '../pages/landing-page'

export default class LandingPageController extends React.Component<any, LandingPageState> {
  togglePreferredShortReference (e: any) {
    this.setState({
      isPreferredShortReferenceAppears: e.target.checked
    })
  }

  async onShortify (e: Event) {
    e.preventDefault()

    const payload = {
      url: this.getRefValue('url'),
      preferredShortReference: this.getRefValue('preferredShortReference') || false
    } as any

    if (!this.isLinkValid(payload.url)) {
      return alert('Link is not Valid')
    }

    const generatedLink = await Fetch('/generate-new-link', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: { 'Content-Type': 'application/json' }
    }).then(res => res.json()) as any

    const shortedLink = Url.resolve(window.location.href.split('?')[0], generatedLink.shortReference)

    this.state.shortedLinks.unshift({
      url: payload.url,
      shortedLink
    })

    this.setState(this.state)
  }

  isLinkValid (url: string) {
    return url.match(/^^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm)
  }

  private getRefValue (ref: string): string {
    const refObject = this.refs[ref] as any

    return refObject ? refObject.value : ''
  }
}
