import * as Path from 'path'
import App from './app'
import { Config } from 'link-shortener'

const nodeEnv = process.env.NODE_ENV || 'development'
const config = require(Path.resolve(__dirname, 'configs', nodeEnv)) as Config

(async () => {
  const app = await App(config)

  app.listen(config.app.servicePort, () => console.log(`app listening on port ${config.app.servicePort}!`))
})()
