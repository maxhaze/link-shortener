import { ServicesList, ModelsList } from 'link-shortener'
import Links from './links'

export default function (models: ModelsList): ServicesList {
  return {
    links: Links(models)
  }
}
