
import { ModelsList, LinksService, LinkModel } from 'link-shortener'

export default function (models: ModelsList): LinksService {
  async function generateNewLink (url: string, shortReference: string): Promise<LinkModel> {
    const generatedLink = await models.links.create({
      url,
      shortReference
    }) as any

    return generatedLink.toJSON() as LinkModel
  }

  async function isShortReferenceExists (shortReference: string): Promise<Boolean> {
    const countResult = await models.links.count({
      where: {
        shortReference
      }
    }) as Number

    return countResult > 0
  }

  async function getLinkByShortReference (shortReference: string): Promise<LinkModel> {
    return await models.links.findOne({
      attributes: ['id', 'url', 'shortReference', 'registeredAt'],
      where: {
        shortReference
      }
    })
  }

  return {
    generateNewLink,
    isShortReferenceExists,
    getLinkByShortReference
  }
}
