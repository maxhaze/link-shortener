import * as Sequelize from 'sequelize'
import * as express from 'express'
import * as BodyParser from 'body-parser'
import * as Path from 'path'

import Models from './models'
import Services from './services'
import Controllers from './controllers'

import { Config, Express } from 'link-shortener'

const app = express() as Express
const nodeEnv = process.env.NODE_ENV || 'development'

export default async (config: Config) => {
  app.use(BodyParser.json())
  app.use(BodyParser.urlencoded({ extended: false }))
  app.use(express.static(Path.resolve(__dirname, '../dist/static')))
  app.set('views', Path.resolve(__dirname, '../views'))
  app.set('view engine', 'pug')

  // loading models
  const sequelize = new Sequelize(`postgres://${config.database.username}:${config.database.password}@` +
    `${config.database.host}:${config.database.port}/${config.database.database}`, {
      operatorsAliases: false,
      omitNull: true,
      logging: nodeEnv === 'development' ? console.log : false
    })

  await sequelize.authenticate()

  // context
  const models = Models(sequelize)
  const services = Services(models)

  await sequelize.sync()

  const controllers = Controllers(services)

  app.sequelize = sequelize
  // add new account
  app.get('/health-check', (_: any, res: any) => {
    res.json({
      success: true
    })
  })

  const handleError = controllers.handleError

  app.post('/generate-new-link', handleError(controllers.links.generateNewLink))
  app.get('/:shortReference', controllers.links.handleShortReference)
  app.get('/', controllers.links.landingPage)

  app.use((_: any, res: any) => {
    res.status(404).render('not-found')
  })

  app.on('close', () => sequelize.close())

  return app
}
