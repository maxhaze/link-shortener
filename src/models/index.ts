import { ModelsList } from 'link-shortener'
import Links from './links'

export default function (sequalize: any): ModelsList {
  return {
    links: Links(sequalize)
  }
}
