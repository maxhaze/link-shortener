import * as Sequelize from 'sequelize'
import { LinksModel } from 'link-shortener'

const modelName = 'links'

export default function (sequelize: any) {
  const model = sequelize.define(modelName, {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    url: {
      field: 'url',
      type: Sequelize.STRING,
      allowNull: false
    },
    shortReference: {
      field: 'short_reference',
      type: Sequelize.STRING,
      allowNull: false
    },
    registeredAt: {
      field: 'registered_at',
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    }
  } as LinksModel, {
    tableName: `links`,
    underscored: true,
    timestamps: true,
    createdAt: 'registeredAt',
    updatedAt: false,
    deletedAt: false
  })

  sequelize[modelName] = sequelize.import(modelName, () => {
    return model
  })

  return model
}
