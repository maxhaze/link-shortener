import { ControllersList, ServicesList } from 'link-shortener'
import { Request, Response } from 'express'
import Links from './links'

export default function (services: ServicesList): ControllersList {
  function handleError(middleware: Function) {
    return async (req: Request, res: Response, next: any) => {
      try {
        return await middleware(req, res, next)
      } catch (error) {
        res.statusCode = 400
        res.json({ error })
      }
    }
  }

  return {
    links: Links(services),
    handleError
  }
}
