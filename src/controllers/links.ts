import * as ShortHash from 'shorthash'
import * as moment from 'moment'
import * as URI from 'urijs'

import { Request, Response } from 'express'
import { ServicesList, LinksController } from 'link-shortener'

export default function (service: ServicesList): LinksController {
  function landingPage (req: Request, res: Response) {
    res.render('landing-page')
  }

  async function handleShortReference (req: Request, res: Response): Promise<void> {
    const { shortReference } = req.params

    const link = await service.links.getLinkByShortReference(shortReference)

    if (!link) {
      return res.status(404).render('not-found.pug')
    }

    res.redirect(link.url)
  }

  async function generateNewLink (req: Request, res: Response) {
    let { url, preferredShortReference} = req.body

    // mutate the payload to keep it clean
    url = url && url.trim()
    preferredShortReference = preferredShortReference && preferredShortReference.replace(/[^\w\s]/gi, '')

    if (!isLinkValid(url)) {
      throw `url<${url}> is not valid`
    }

    // generate a unique hash code for short reference
    let shortReference = ShortHash.unique(`${url}${moment().toDate()}${Math.random()}`)

    if (preferredShortReference && !await service.links.isShortReferenceExists(preferredShortReference)) {
      shortReference = preferredShortReference
    }

    const generatedLink = await service.links.generateNewLink(url, shortReference)

    res.json(generatedLink)
  }

  function isLinkValid(url: string) {
    return url.match(/^^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm)
  }

  return {
    landingPage,
    handleShortReference,
    generateNewLink
  }
}
