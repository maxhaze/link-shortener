import { Express, RequestHandler } from 'express'


export interface Config {
  app: {
    servicePort: number
  },
  database: {
    host: string
    database: string
    port: number
    username: string
    password: string
  }
}

export interface Express extends Express {
  close: Function
  sequelize: any
}

export interface ModelsList {
  links: any
}

export interface ServicesList {
  links: LinksService
}

export interface ControllersList {
  links: LinksController
  handleError: Function
}

// Models
export interface LinksModel {
  id: any
  url: any
  shortReference: any
  registeredAt: any
}

export interface LinkModel {
  url: string
  shortReference: string
  registeredAt: string
}

// Services
export interface LinksService {
  generateNewLink: Function
  isShortReferenceExists: Function
  getLinkByShortReference: Function
}

// Controllers
export interface LinksController {
  landingPage: RequestHandler
  handleShortReference: RequestHandler
  generateNewLink: RequestHandler
}
