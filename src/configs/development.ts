import { Config } from 'link-shortener'

module.exports = {
  app: {
    servicePort: 4000
  },
  database: {
    host: process.env.DB_HOST || '0.0.0.0',
    database: process.env.DB_DATABASE || 'linkshortener',
    port: process.env.DB_PORT || 5432,
    username: process.env.DB_USERNAME || 'linkshortener',
    password: process.env.DB_PASSWORD || 'linkshortener'
  }
} as Config
