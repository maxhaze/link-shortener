import { Config } from 'link-shortener'

module.exports = {
  app: {
    servicePort: 4000
  },
  database: {
    host: process.env.TEST_ENV === 'ci' ? 'postgres' : '0.0.0.0',
    database: 'linkshortener',
    port: 5432,
    username: 'linkshortener',
    password: 'linkshortener'
  }
} as Config
