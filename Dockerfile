FROM node:8.11

WORKDIR /app

ADD . /app

EXPOSE 4000

RUN npm i && npm run build

# compile tsx files
RUN npm run webpack

# start the service
CMD [ "npm", "start" ]

LABEL Name="link-shortener" \
      Version="v0.4"
