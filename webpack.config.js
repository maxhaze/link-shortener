module.exports = {
  entry: "./ui/app.tsx",
  mode: process.env.NODE_ENV || 'development',
  output: {
      filename: "app.js",
      path: __dirname + "/dist/static"
  },
  devtool: "source-map",
  resolve: {
      extensions: [".ts", ".tsx", ".js", ".json"]
  },
  module: {
      rules: [
          // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
          { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
          // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
          { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
      ]
  },
  externals: {
      "react": "React",
      "react-dom": "ReactDOM"
  }
};